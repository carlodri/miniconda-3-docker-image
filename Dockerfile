FROM phusion/baseimage:0.11

ENV PATH=/opt/conda/bin:$PATH \
    LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash","-c"]

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

RUN apt-get update --fix-missing && \
    apt-get install -y wget bzip2 ca-certificates git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-4.5.12-Linux-x86_64.sh -O ~/miniconda.sh && \
    chmod +x ~/miniconda.sh && \
    ~/miniconda.sh -b -p /opt/conda && \
    rm -f ~/miniconda.sh && \
    /opt/conda/bin/conda clean --all --yes
    
RUN mkdir -p /etc/my_init.d && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/my_init.d/conda.sh && \
    echo "#!/bin/bash" >> /etc/rc.local && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> /etc/rc.local && \
    echo "conda activate base" >> /etc/rc.local && \
    chmod +x /etc/rc.local